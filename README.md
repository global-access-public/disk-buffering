# Disk Buffering for haskell values

Buffering operations based on vectors of serializables

Supports asynchronous access

```haskell 
{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

import Control.Monad.Catch (try)
import Data.Buffering (bufferIn, bufferOut)
import Protolude (IOException, async, poll, threadDelay, void)
import Streaming (lift)
import qualified Streaming.Prelude as S
import System.Directory (doesDirectoryExist, removeDirectoryRecursive)
import Test.Hspec (describe, hspec, it, shouldBe)

main :: IO ()
main = hspec $ do
  describe "buffering" $ do
    it "works synchronously " $ do
      void $ try @_ @IOException $ removeDirectoryRecursive "bufferDir"
      let xs :: [Int] = [1 .. 10_000]
      _ <- S.effects $ bufferIn "bufferDir" 100 $ S.each xs
      rs <- S.toList_ $ S.effects $ bufferOut "bufferDir"
      rs `shouldBe` xs
    it "works asynchronously " $ do
      void $ try @_ @IOException $ removeDirectoryRecursive "bufferDir"
      let xs :: [Int] = [1 .. 10_000]
      w <- async $ S.effects $ bufferIn "bufferDir" 100 $ S.each xs
      let r = S.effects $ bufferOut "bufferDir"
          go = do
            t <- lift $ doesDirectoryExist "bufferDir"
            if not t
              then lift (threadDelay 1_000) >> go
              else do
                z <- lift $ poll w
                case z of
                  Nothing -> r >> go
                  Just _ -> r
      rs <- S.toList_ go
      rs `shouldBe` xs
```
